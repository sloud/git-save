gs() {
    local comment="$@"

    git status
    git add -A
    git commit -am "$comment"
    git push
}